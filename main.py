#!/usr/bin/env python3
from datetime import datetime, timedelta
from dotenv import load_dotenv
import os
import taiga
from dateutil.relativedelta import relativedelta

load_dotenv()  # take environment variables from .env.


class DotDict(dict):
    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self[key] = value


class PersonalKanban:

    RECURRING_DAILY = "Daily"
    RECURRING_WEEKLY = "Weekly"
    RECURRING_MONTHLY = "Monthly"

    def __init__(self):
        self.api = taiga.TaigaAPI(
            host=os.getenv("TAIGA_HOST")
        )
        self.api.auth(
            username=os.getenv("TAIGA_USERNAME"),
            password=os.getenv("TAIGA_PASSWORD")
        )
        self.project = self.api.projects.get_by_slug(
            os.getenv("TAIGA_PROJECT_NAME"))

        self._init_user_story_statuses()

        self.recurring_attribute = self.project.list_user_story_attributes().filter(
            name="Recurring")[0]

        userstory_due_dates = self._get_userstory_due_dates()
        due_soon = [
            entry for entry in userstory_due_dates if entry["name"] == "Due soon"]
        self.DUE_SOON_DAYS = due_soon[0]["days_to_due"]

    def _init_user_story_statuses(self):
        statuses = self.project.list_user_story_statuses()
        self.status = DotDict()
        self.status.backlog = statuses.filter(name="Backlog")[0]
        self.status.prio = statuses.filter(name="Prio")[0]
        self.status.wip = statuses.filter(name="WIP")[0]
        self.status.done = statuses.filter(name="Done")[0]
        self.status.archived = statuses.filter(name="Archived")[0]

    def _get_userstory_due_dates(self):
        # Seems like this is not part of the python package nor the official API documentation
        # so we make a raw request
        return self.api.raw_request.get(
            "/userstory-due-dates?project={project_id}", project_id=self.project.id).json()

    def _get_user_stories_done(self):
        return self.project.list_user_stories(status=self.status.done.id)

    def _get_open_user_stories_with_due_date(self):
        stories = []
        for story in self.project.list_user_stories(status=self.status.backlog.id):
            if story.due_date:
                stories.append(story)
        return stories

    def _get_recurring_user_stories_done(self):
        for story in self._get_user_stories_done():
            attribute = story.get_attributes()
            if attribute["attributes_values"]:
                yield story, attribute["attributes_values"]

    def archive_done_stories(self):
        done_stories = self._get_user_stories_done()
        for story in done_stories:
            print(f"Archiving story id {story.ref}")
            story.status = self.status.archived.id
            story.patch(["status"], version=story.version)

    def prioritize_upcoming_deadlines(self):
        stories = self._get_open_user_stories_with_due_date()
        for story in stories:
            due_date = datetime.strptime(story.due_date, "%Y-%m-%d").date()
            if due_date <= datetime.now().date() + timedelta(days=self.DUE_SOON_DAYS):
                print(f"Upcoming deadline for story id {story.ref}")
                story.status = self.status.prio.id
                story.patch(["status"], version=story.version)

    def put_recurring_stories_in_backlog(self):
        for story, attributes in self._get_recurring_user_stories_done():
            due_date = datetime.strptime(story.due_date, "%Y-%m-%d").date()
            if attributes[str(self.recurring_attribute.id)] == PersonalKanban.RECURRING_MONTHLY:
                new_due_date = due_date + relativedelta(months=1)
            elif attributes[str(self.recurring_attribute.id)] == PersonalKanban.RECURRING_WEEKLY:
                new_due_date = due_date + relativedelta(weeks=1)
            elif attributes[str(self.recurring_attribute.id)] == PersonalKanban.RECURRING_DAILY:
                new_due_date = due_date + relativedelta(days=1)
            else:
                continue
            print(
                f"Putting recurring story id {story.ref} in backlog, new due date {new_due_date}")
            story.due_date = new_due_date.strftime("%Y-%m-%d")
            story.status = self.status.backlog.id
            story.patch(["due_date", "status"], version=story.version)


def main():
    personal_kanban = PersonalKanban()

    if os.getenv("JOB_BACKLOG_RECURRING_STORIES"):
        print("Put recurring stories in backlog...")
        personal_kanban.put_recurring_stories_in_backlog()
        print("...done")

    if os.getenv("JOB_ARCHIVE_DONE_STORIES"):
        print("Archiving done stories...")
        personal_kanban.archive_done_stories()
        print("...done")

    if os.getenv("JOB_PRIORITIZE_UPCOMING_DEADLINES"):
        print("Prioritizing upcoming deadlines...")
        personal_kanban.prioritize_upcoming_deadlines()
        print("...done")

    print("Finished all jobs")


if __name__ == "__main__":
    main()
